﻿using OpenCvSharp;
using OpenCvSharp.Extensions;
using System;
using System.Drawing;
using System.Linq;

namespace TrafficLightsLib
{
    public class ObjectDetection
    {
        private readonly string fileCascade = Environment.CurrentDirectory + @"\Resources\cascade.xml";

        public Mat DetectAndDisplay(Mat frame, CascadeClassifier cascadeClassifier)
        {
            using (Mat frameGray = new Mat())
            {
                Cv2.CvtColor(frame, frameGray, ColorConversionCodes.BGR2GRAY);
                Cv2.EqualizeHist(frameGray, frameGray);

                var trafficLights = cascadeClassifier.DetectMultiScale(frame);
                foreach (var trafficLight in trafficLights)
                {
                    using (Mat crop = new Mat(frame, new Rect(trafficLight.Location, trafficLight.Size)))
                    {
                        Cv2.Rectangle(frame, new Rect(trafficLight.Location, trafficLight.Size), new Scalar(255, 0, 255));
                        Cv2.PutText(frame, DetectColor(crop), new OpenCvSharp.Point(trafficLight.Location.X - 10, trafficLight.Y + trafficLight.Height + 35), HersheyFonts.HersheySimplex, 1, new Scalar(255, 0, 255));
                    }
                }
                return frame;
            }
        }

        public Bitmap Start(string path)
        {
            using (CascadeClassifier cascadeClassifier = new CascadeClassifier(fileCascade))
            using (Mat trafficLights = new Mat(path, ImreadModes.AnyDepth | ImreadModes.AnyColor))
            using (Mat image = DetectAndDisplay(trafficLights, cascadeClassifier))
            {
                return BitmapConverter.ToBitmap(image);
            }
        }

        public string DetectColor(Mat frame)
        {
            using (Mat maskRed = new Mat())
            using (Mat maskYellow = new Mat())
            using (Mat maskGreen = new Mat())
            using (Mat hsv = new Mat())
            {
                double lenRed = 0;
                double lenYellow = 0;
                double lenGreen = 0;
                Cv2.CvtColor(frame, hsv, ColorConversionCodes.BGR2HSV);
                Cv2.InRange(hsv, new Scalar(110, 150, 150), new Scalar(180, 255, 255), maskRed);
                Cv2.InRange(hsv, new Scalar(15, 150, 150), new Scalar(35, 255, 255), maskYellow);
                Cv2.InRange(hsv, new Scalar(55, 78, 64), new Scalar(90, 255, 255), maskGreen);
                Cv2.FindContours(maskRed,
                                 out var contours,
                                 out var hierarchy,
                                 RetrievalModes.Tree,
                                 ContourApproximationModes.ApproxSimple);
                foreach (var contour in contours)
                {
                    lenRed += contour.Length;
                }
                Cv2.FindContours(maskYellow,
                                 out contours,
                                 out hierarchy,
                                 RetrievalModes.Tree,
                                 ContourApproximationModes.ApproxSimple);
                foreach (var contour in contours)
                {
                    lenYellow += contour.Length;
                }
                Cv2.FindContours(maskGreen,
                                 out contours,
                                 out hierarchy,
                                 RetrievalModes.Tree,
                                 ContourApproximationModes.ApproxSimple);
                foreach (var contour in contours)
                {
                    lenGreen += contour.Length;
                }
                string l = new[] { Tuple.Create(lenRed, "Red"), Tuple.Create(lenYellow, "Yellow"), Tuple.Create(lenGreen, "Green") }.Max().Item2;
                return l;
            }
        }
    }
}
