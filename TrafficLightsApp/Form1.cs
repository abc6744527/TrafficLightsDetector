﻿using System;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using TrafficLightsLib;

namespace TrafficLightsApp
{
    public partial class Form1 : Form
    {
        private Bitmap image;
        private string filePath;
        private readonly ObjectDetection ObjectDetection = new ObjectDetection();
        protected Thread getImageThread;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DragEnter += (s, a) =>
            {
                var data = a.Data.GetData("FileDrop");
                if (data != null)
                {
                    string fileName = ((string[])data)[0];
                    string ext = Path.GetExtension(fileName).ToLower();
                    if ((ext == ".jpg") || (ext == ".png") || (ext == ".bmp"))
                    {
                        Thread getImageThread = new Thread(new ThreadStart(() => LoadImage(fileName)));
                        getImageThread.Start();
                        a.Effect = DragDropEffects.Copy;
                        filePath = fileName;
                    }
                }
                else
                {
                    a.Effect = DragDropEffects.None;
                }
            };
            DragDrop += (s, a) =>
            {
                pictureBox1.Image = image;
                textBox1.Text = filePath;
            };
            button1.Click += (s, a) =>
            {
                openFileDialog1.ShowDialog();
                filePath = openFileDialog1.FileName;
                if (!string.IsNullOrEmpty(filePath))
                {
                    textBox1.Text = filePath;
                    getImageThread = new Thread(new ThreadStart(() => LoadImage(filePath)));
                    getImageThread.Start();
                    pictureBox1.Image = image;
                }
            };
            button2.Click += (s, a) =>
            {
                if (!string.IsNullOrEmpty(filePath))
                {
                    pictureBox1.Image = ObjectDetection.Start(textBox1.Text);
                }
                else
                {
                    MessageBox.Show("Необходимо указать путь к файлу");
                }
            };
        }

        protected void LoadImage(string path)
        {
            Stream stream = new FileStream(path, FileMode.Open, FileAccess.Read);
            image = new Bitmap(stream);
            stream.Close();
        }
    }
}
